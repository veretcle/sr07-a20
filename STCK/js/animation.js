window.onload = function () {
	var s1 = Snap("#RAID0");
	Snap.load("img/schema_RAID0.svg", function(f) {
		var data1 = f.select('#data1');
		var data2 = f.select('#data2');
		var data3 = f.select('#data3');
		var data4 = f.select('#data4');
		data1.click(function() {
			data1.animate({transform:'t0,200'},500,mina.linear,function(){
			data1.animate({transform:'t-200,200'},500,mina.linear,function(){
			data1.animate({transform:'t-200,100'},500,mina.linear,function(){
			data2.animate({transform:'t0,200'},500,mina.linear,function(){
			data2.animate({transform:'t200,200'},500,mina.linear,function(){
			data2.animate({transform:'t200,100'},500,mina.linear,function(){
			data3.animate({transform:'t0,200'},500,mina.linear,function(){
			data3.animate({transform:'t-200,200'},500,mina.linear,function(){
			data3.animate({transform:'t-200,300'},500,mina.linear,function(){
			data4.animate({transform:'t0,200'},500,mina.linear,function(){
			data4.animate({transform:'t200,200'},500,mina.linear,function(){
			data4.animate({transform:'t200,300'},500,mina.linear);
			});
			});
			});
			});
			});
			});
			});
			});
			});
			});
			});
		});
		s1.append(f);
	});
	var s2 = Snap("#RAID1");
	Snap.load("img/schema_RAID1.svg", function(f) {
		var data1 = f.select("#data1");
		var data2 = f.select("#data2");
		data1.click(function() {
			data1.animate({transform:'t0,200'},500,mina.linear,function(){
				var data1_c = data1.clone();
				data1.animate({transform:'t-200,200'},500,mina.linear,function(){
				data1.animate({transform:'t-200,100'},500,mina.linear);
				});
				data1_c.animate({transform:'t200,200'},500,mina.linear,function(){
				data1_c.animate({transform:'t200,100'},500,mina.linear);
				});
			});
		});
		data2.click(function() {
			data2.animate({transform:'t0,200'},500,mina.linear,function(){
				var data2_c = data2.clone();
				data2.animate({transform:'t-200,200'},500,mina.linear,function(){
				data2.animate({transform:'t-200,300'},500,mina.linear);
				});
				data2_c.animate({transform:'t200,200'},500,mina.linear,function(){
				data2_c.animate({transform:'t200,300'},500,mina.linear);
				});
			});
		});
		s2.append(f);
	});
	var s3 = Snap("#RAID5");
	Snap.load("img/schema_RAID5.svg", function(f) {
		var data1 = f.select("#data1");
		var data2 = f.select("#data2");
		var xor12 = f.select("#xor12");
		xor12.attr({ visibility: "hidden" });
		data1.click(function(){
			data1.animate({transform:'t0,150'},500,mina.linear,function(){
			data2.animate({transform:'t0,200'},500,mina.linear);
			});
		});
		data2.click(function(){
			xor12.attr({ visibility: "" });
		});
		xor12.click(function(){
			data1.animate({transform:'t-200,300'},500,mina.linear);
			data2.animate({transform:'t0,300'},500,mina.linear);
			xor12.animate({transform:'t-120,50'},500,mina.linear,function(){
			xor12.animate({transform:'t50,180'},500,mina.linear)
			});
		});
		s3.append(f);
	});
	var s4 = Snap("#LVM_PRINCIPE");
	Snap.load("img/lvm_principe.svg", function(f){
		s4.append(f);
	});
	var s5 = Snap("#LVM_AGRANDISSEMENT");
	Snap.load("img/lvm_agrandissement.svg", function(f) {
		var sda = f.select('#sda');
		var sdb = f.select('#sdb');
		var vg00 = f.select('#vg00');
		var root = f.select('#root');
		var home = f.select('#home');
		sdb.attr({ display: "none"});
		sda.click(function() {
			sdb.attr({ display: "inline" });
		});
		sdb.click(function() {
			vg00.animate({transform:'s1.5r0,0,0'},500,mina.linear);
		});
		vg00.click(function() {
			root.animate({transform:'t320,-180'},500,mina.linear,function(){
			home.animate({transform:'t260,-60s1.2,1.2,0,0'},500,mina.linear);
			});
		});
		s5.append(f);
	var s6 = Snap("#snapshot");
	Snap.load("img/snapshot.svg", function(f) {
		// état initial
		var percent_125 = f.select('#percent_125');
		var percent_0 = f.select('#percent_0');
		var snapshot = f.select('#snapshot');
		var field_appear = f.select('#field_appear');
		var bit_appear = f.select('#bit_appear');
		percent_125.attr({ display: 'none' });
		percent_0.attr({ display: 'none' });
		snapshot.attr({ display: 'none' });
		field_appear.attr({ display: 'none'});
		bit_appear.attr({ display: 'none'});
		// snapshot
		var orig = f.select('#orig');
		orig.click(function() {
		    snapshot.attr({ display: 'inline' });
		    percent_0.attr({ display: 'inline' });
		});
		var bit_disappear = f.select('#bit_disappear');
		var arrow_disappear = f.select('#arrow_disappear');
		bit_disappear.click(function() {
			bit_disappear.attr({ display: 'none' });
			bit_appear.attr({ display: 'inline' });
			field_appear.attr({ display: 'inline' });
			arrow_disappear.attr({ display: 'none' });
			percent_0.attr({ display: 'none' });
			percent_125.attr({ display: 'inline' });
		});
		s6.append(f);
	});
	var s7 = Snap("#nas");
	Snap.load("img/nas.svg", function(f) {
		s7.append(f);
	});
	var s8 = Snap("#san");
	Snap.load("img/san.svg", function(f) {
		s8.append(f);
	});

	var s9 = Snap("#heritage");
	Snap.load("img/héritage.svg", function(f) {
		s9.append(f);
	});

	var s10 = Snap("#iscsi");
	Snap.load("img/iscsi.svg", function(f) {
		var target_title = f.select('#target_title');
		var decap_eth = f.select('#decap_eth');
		var decap_ip = f.select('#decap_ip');
		var decap_ipsec = f.select('#decap_ipsec');
		var decap_scsi = f.select('#decap_scsi_2');
		var encap_sata = f.select('#encap_sata');
		var data_rcv = f.select('#data_rcv');
		var arrow_1 = f.select('#arrow_1');
		var arrow_2 = f.select('#arrow_2');
		var disk = f.select('#disk');
		var encap_eth = f.select('#encap_eth');
		var encap_ip = f.select('#encap_ip');
		var encap_ipsec = f.select('#encap_ipsec');
		var encap_scsi = f.select('#encap_scsi');
		var data = f.select('#data');
		// disappear
		target_title.attr({ display: 'none' });
		decap_eth.attr({ display: 'none' });
		decap_ip.attr({ display: 'none' });
		decap_ipsec.attr({ display: 'none' });
		decap_scsi.attr({ display: 'none' });
		encap_sata.attr({ display: 'none' });
		data_rcv.attr({ display: 'none' });
		arrow_1.attr({ display: 'none' });
		arrow_2.attr({ display: 'none' });
		disk.attr({ display: 'none' });
		encap_eth.attr({ display: 'none' });
		encap_ip.attr({ display: 'none' });
		encap_ipsec.attr({ display: 'none' });
		encap_scsi.attr({ display: 'none' });
		data.click(function() {
			encap_scsi.attr({ display: 'inline' });
		});
		encap_scsi.click(function() {
			encap_ipsec.attr({ display: 'inline' });
		});
		encap_ipsec.click(function() {
			encap_ip.attr({ display: 'inline' });
		});
		encap_ip.click(function() {
			encap_eth.attr({ display: 'inline' });
		});
		encap_eth.click(function() {
			target_title.attr({ display: 'inline' });
			arrow_1.attr({ display: 'inline' });
			data_rcv.attr({ display: 'inline' });
			decap_scsi.attr({ display: 'inline' });
			decap_ipsec.attr({ display: 'inline' });
			decap_ip.attr({ display: 'inline' });
			decap_eth.attr({ display: 'inline' });
		});
		decap_eth.click(function() {
			decap_eth.attr({ display: 'none' });
		});
		decap_ip.click(function() {
			decap_ip.attr({ display: 'none' });
		});
		decap_ipsec.click(function() {
			decap_ipsec.attr({ display: 'none' });
		});
		decap_scsi.click(function() {
			decap_scsi.attr({ display: 'none' });
		});
		data_rcv.click(function() {
			encap_sata.attr({ display: 'inline' });
		});
		encap_sata.click(function() {
			arrow_2.attr({ display: 'inline' });
			disk.attr({ display: 'inline' });
		});
		s10.append(f);
	});

	var s11 = Snap('#aoe');
	Snap.load("img/aoe.svg", function(f) {
		var target_title = f.select('#target_title');
		var decap_eth = f.select('#decap_eth');
		var data_rcv = f.select('#data_rcv');
		var arrow_1 = f.select('#arrow_1');
		var arrow_2 = f.select('#arrow_2');
		var disk = f.select('#disk');
		var encap_eth = f.select('#encap_eth');
		var encap_ata = f.select('#encap_ata');
		var data = f.select('#data');
		target_title.attr({ display: 'none' });
		decap_eth.attr({ display: 'none' });
		data_rcv.attr({ display: 'none' });
		arrow_1.attr({ display: 'none' });
		arrow_2.attr({ display: 'none' });
		disk.attr({ display: 'none' });
		encap_eth.attr({ display: 'none' });
		encap_ata.attr({ display: 'none' });
		data.click(function() {
			encap_ata.attr({ display: 'inline' });
		});
		encap_ata.click(function() {
			encap_eth.attr({ display: 'inline' });
		});
		encap_eth.click(function() {
			target_title.attr({ display: 'inline' });
			arrow_1.attr({ display: 'inline' });
			decap_eth.attr({ display: 'inline' });
			data_rcv.attr({ display: 'inline' });
		});
		decap_eth.click(function() {
			decap_eth.attr({ display: 'none' });
		});
		data_rcv.click(function() {
			arrow_2.attr({ display: 'inline' });
			disk.attr({ display: 'inline' });
		});
	s11.append(f);
	});

	});
};
